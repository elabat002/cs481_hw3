import 'package:flutter/material.dart';
import 'package:flutter_app/MyBackgroundColor.dart';
import 'package:flutter_app/MyBorderColor.dart';
import 'package:flutter_app/MyBorderRadius.dart';
import 'package:flutter_app/MyTextLabel.dart';
import 'package:flutter_app/MyButton.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePage> {

  double _width;
  double _height;
  Border _border;
  BorderRadius _borderRadius;
  Color _color;
  String _text;
  Color _textColor;

  @override
  void initState() {
    super.initState();
    _width = 167;
    _height = 52;
    _border = Border.all(width: 2, color: Colors.black54);
    _borderRadius = BorderRadius.circular(0);
    _color = Colors.white;
    _text = 'Button';
    _textColor = Colors.black;
  }

  void setBackgroundColor(Color color) {
    setState(() {
      _color = color;
    });
  }

  void setBorderColor(Color color) {
    setState(() {
      _border = Border.all(width: 2, color: color);
    });
  }

  void setBorderRadius(double radius) {
    setState(() {
      _borderRadius = BorderRadius.circular(radius);
    });
  }

  void setTextLabel(String text) {
    setState(() {
      _text = text;
    });
  }

  void setTextColor(Color color) {
    setState(() {
      _textColor = color;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 10),
          child: Text('Play with settings to create the button of your dreams !', style: TextStyle(
              fontFamily: "Comfortaa",
              fontWeight: FontWeight.bold,
              fontSize: 20
            )
          ),
        ),
        MyBackgroundColor(setBackgroundColor),
        MyBorderColor(setBorderColor),
        MyBorderRadius(setBorderRadius),
        MyTextLabel(setTextLabel, setTextColor),
        MyButton(
            _width,
            _height,
            BoxDecoration(border: _border, borderRadius: _borderRadius),
            _color,
            Text(_text, style: TextStyle(fontFamily: "Comfortaa", color: _textColor)),
            null
        )
      ]
    );
  }
}