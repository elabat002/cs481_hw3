import 'package:flutter/material.dart';

import 'package:flutter_app/MyButton.dart';

class MyBackgroundColor extends StatelessWidget {

  MyBackgroundColor(this.setBackgroundColor);

  final Function setBackgroundColor;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget> [
        Padding(
            padding: const EdgeInsets.only(left: 10, top: 20),
            child: Text('Background color :', style: TextStyle(
                fontFamily: "Comfortaa",
                fontWeight: FontWeight.bold
            )
            )
        ),
        Padding(
            padding: const EdgeInsets.only(top: 15, left: 10),
            child: MyButton(20, 20, BoxDecoration(borderRadius: BorderRadius.circular(50)), Colors.redAccent, null, () {setBackgroundColor(Colors.redAccent);})
        ),
        Padding(
            padding: const EdgeInsets.only(top: 15, left: 10),
            child: MyButton(20, 20, BoxDecoration(borderRadius: BorderRadius.circular(50)), Colors.greenAccent, null, () {setBackgroundColor(Colors.greenAccent);})
        ),
        Padding(
            padding: const EdgeInsets.only(top: 15, left: 10),
            child: MyButton(20, 20, BoxDecoration(borderRadius: BorderRadius.circular(50)), Colors.blueAccent, null, () {setBackgroundColor(Colors.blueAccent);})
        ),
        Padding(
          padding: const EdgeInsets.only(top: 15, left: 10),
          child: IconButton(
              icon: Icon(Icons.restore),
              onPressed: () {
                setBackgroundColor(Colors.white);
              }
          ),
        )
      ],
    );
  }
}