import 'package:flutter/material.dart';

import 'package:flutter_app/MyButton.dart';

class MyBorderColor extends StatelessWidget {

  MyBorderColor(this.setBorderColor);

  final Function setBorderColor;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Padding(
            padding: const EdgeInsets.only(left: 10, top: 20),
            child: Text('Border color :', style: TextStyle(
                fontFamily: "Comfortaa",
                fontWeight: FontWeight.bold
            )
            )
        ),
        Padding(
            padding: const EdgeInsets.only(top: 15, left: 10),
            child: MyButton(
                20, 20, BoxDecoration(borderRadius: BorderRadius.circular(50)),
                Colors.red[800], null, () {
              setBorderColor(Colors.red[800]);
            })
        ),
        Padding(
            padding: const EdgeInsets.only(top: 15, left: 10),
            child: MyButton(
                20, 20, BoxDecoration(borderRadius: BorderRadius.circular(50)),
                Colors.green, null, () {
              setBorderColor(Colors.green);
            })
        ),
        Padding(
            padding: const EdgeInsets.only(top: 15, left: 10),
            child: MyButton(
                20, 20, BoxDecoration(borderRadius: BorderRadius.circular(50)),
                Colors.blue[800], null, () {
              setBorderColor(Colors.blue[800]);
            })
        ),
        Padding(
          padding: const EdgeInsets.only(top: 15, left: 10),
          child: IconButton(
            icon: Icon(Icons.restore),
            onPressed: () {
              setBorderColor(Colors.black54);
            }
          ),
        )
      ],
    );
  }
}