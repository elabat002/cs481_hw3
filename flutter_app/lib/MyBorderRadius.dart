import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class MyBorderRadius extends StatefulWidget {

  MyBorderRadius(this.setBorderRadius, {Key key}) : super(key: key);

  final Function setBorderRadius;

  @override
  _MyBorderRadiusState createState() => _MyBorderRadiusState();
}

class _MyBorderRadiusState extends State<MyBorderRadius> {

  final TextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget> [
        Padding(
            padding: const EdgeInsets.only(left: 10, top: 20),
            child: Text('Border radius :', style: TextStyle(
                fontFamily: "Comfortaa",
                fontWeight: FontWeight.bold
              )
            )
        ),
        Padding(
          padding: const EdgeInsets.only(left: 10),
          child: SizedBox(
            width: 25,
            height: 30,
            child: TextField(
                maxLength: 2,
                decoration: InputDecoration(counterText: ""),
                controller: TextController,
                onChanged: (input) {
                  widget.setBorderRadius(double.parse(TextController.text));
                },
                keyboardType: TextInputType.number,
                inputFormatters: <TextInputFormatter>[
                 FilteringTextInputFormatter.digitsOnly
              ], // Only numbers c
            ),
          ),
        )
      ],
    );
  }
}