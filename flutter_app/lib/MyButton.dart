import 'package:flutter/material.dart';

class MyButton extends StatelessWidget {
  MyButton(
      this._width,
      this._height,
      this._boxDecoration,
      this._color,
      this._text,
      this._onTap
      );

  final double _width;
  final double _height;
  final BoxDecoration _boxDecoration;
  final Color _color;
  final Text _text;
  final Function _onTap;

  @override
  Widget build(BuildContext context) {
    return Material(
      color: _color,
      borderRadius: _boxDecoration.borderRadius,
      child: InkWell(
          onTap: _onTap,
          child: Container(
              width: _width,
              height: _height,
              decoration: _boxDecoration,
              child: Center(
                child: _text,
              )
          )
      ),
    );
  }
}