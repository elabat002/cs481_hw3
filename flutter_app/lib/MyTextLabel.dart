import 'package:flutter/material.dart';
import 'package:flutter_app/MyButton.dart';

class MyTextLabel extends StatefulWidget {
  MyTextLabel(this.setTextLabel, this.setTextColor, {Key key}) : super(key: key);

  final Function setTextLabel;
  final Function setTextColor;

  @override
  MyTextLabelState createState() => MyTextLabelState();
}

class MyTextLabelState extends State<MyTextLabel> {
  final TextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget> [
        Padding(
            padding: const EdgeInsets.only(left: 10, top: 20),
            child: Text('Text value :', style: TextStyle(
                fontFamily: "Comfortaa",
                fontWeight: FontWeight.bold
            )
            )
        ),
        Padding(
          padding: const EdgeInsets.only(left: 10),
          child: SizedBox(
            width: 140,
            height: 30,
            child: TextField(
              maxLength: 18,
              decoration: InputDecoration(counterText: "", labelStyle: TextStyle(fontFamily: "Comfortaa")),
              controller: TextController,
              onChanged: (input) {
                widget.setTextLabel(TextController.text.toString());
              },
            ),
          ),
        ),
        Padding(
            padding: const EdgeInsets.only(top: 15, left: 10),
            child: MyButton(
                20, 20, BoxDecoration(borderRadius: BorderRadius.circular(50), border: Border.all(color: Colors.grey)),
                Colors.white, null, () {
              widget.setTextColor(Colors.white);
            })
        ),
        Padding(
            padding: const EdgeInsets.only(top: 15, left: 10),
            child: MyButton(
                20, 20, BoxDecoration(borderRadius: BorderRadius.circular(50)),
                Colors.black, null, () {
              widget.setTextColor(Colors.black);
            })
        ),
        Padding(
          padding: const EdgeInsets.only(top: 15, left: 10),
          child: IconButton(
              icon: Icon(Icons.restore),
              onPressed: () {
                TextController.clear();
                widget.setTextColor(Colors.black);
              }
          ),
        )
      ],
    );
  }
}