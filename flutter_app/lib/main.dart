import 'package:flutter/material.dart';
import 'package:flutter_app/HomePage.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Homework 3',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Homework 3 - Pimp your button !', style: TextStyle(
              fontFamily: "Comfortaa",
              fontWeight: FontWeight.bold
            )
          ),
        ),
        body: HomePage()
      ),
    );
  }
}